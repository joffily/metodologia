from django.core.urlresolvers import reverse_lazy
from .models import Produto
from .forms import ProdutoForm
from vanilla import CreateView, DeleteView, ListView, UpdateView


class ListProdutos(ListView):
    model = Produto


class CreateProduto(CreateView):
    model = Produto
    form_class = ProdutoForm
    success_url = reverse_lazy('list_produtos')


class EditProduto(UpdateView):
    model = Produto
    form_class = ProdutoForm
    success_url = reverse_lazy('list_produtos')


class DeleteProduto(DeleteView):
    model = Produto
    success_url = reverse_lazy('list_produtos')
