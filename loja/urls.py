from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^$', ListProdutos.as_view(), name='list_produtos'),
    url(r'^create/$', CreateProduto.as_view(), name='create_produto'),
    url(r'^edit/(?P<pk>\d+)$', EditProduto.as_view(), name='edit_produto'),
    url(r'^delete/(?P<pk>\d+)$', DeleteProduto.as_view(), name='delete_produto'),
]
